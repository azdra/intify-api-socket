-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Generation Time: Jul 06, 2021 at 07:47 PM
-- Server version: 10.5.10-MariaDB-1:10.5.10+maria~focal
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `intify`
--

-- --------------------------------------------------------

--
-- Table structure for table `channel`
--

CREATE TABLE `channel` (
  `createdAt` datetime NOT NULL,
  `updateAt` datetime DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `topic` varchar(100) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT 1,
  `parent` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `guildId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `channel`
--

INSERT INTO `channel` (`createdAt`, `updateAt`, `id`, `name`, `topic`, `type`, `parent`, `order`, `guildId`) VALUES
('2021-07-06 19:41:47', NULL, '177837263051292672', 'general', NULL, 1, NULL, NULL, '177837262963212288'),
('2021-07-06 19:41:55', NULL, '177837295271936000', 'general', NULL, 1, NULL, NULL, '177837295204827136'),
('2021-07-06 19:42:00', NULL, '177837318441271296', 'general', NULL, 1, NULL, NULL, '177837318348996608'),
('2021-07-06 19:42:04', NULL, '177837335868604416', 'general', NULL, 1, NULL, NULL, '177837335780524032'),
('2021-07-06 19:42:08', NULL, '177837351349780480', 'general', NULL, 1, NULL, NULL, '177837351236534272'),
('2021-07-06 19:42:13', NULL, '177837369766969344', 'general', NULL, 1, NULL, NULL, '177837369699860480'),
('2021-07-06 19:42:19', NULL, '177837397428404224', 'general', NULL, 1, NULL, NULL, '177837397336129536'),
('2021-07-06 19:42:24', NULL, '177837416843837440', 'general', NULL, 1, NULL, NULL, '177837416768339968'),
('2021-07-06 19:43:08', NULL, '177837604379557888', 'general', NULL, 1, NULL, NULL, '177837604186619904');

-- --------------------------------------------------------

--
-- Table structure for table `friend`
--

CREATE TABLE `friend` (
  `createdAt` datetime NOT NULL,
  `updateAt` datetime DEFAULT NULL,
  `id` int(11) NOT NULL,
  `accepted` tinyint(4) NOT NULL DEFAULT 0,
  `userId` varchar(255) DEFAULT NULL,
  `friendId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `guild`
--

CREATE TABLE `guild` (
  `createdAt` datetime NOT NULL,
  `updateAt` datetime DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `ownerId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `guild`
--

INSERT INTO `guild` (`createdAt`, `updateAt`, `id`, `name`, `ownerId`) VALUES
('2021-07-06 19:41:47', NULL, '177837262963212288', 'La Sainte P4', '177837092070490112'),
('2021-07-06 19:41:55', NULL, '177837295204827136', 'Simplon', '177837092070490112'),
('2021-07-06 19:42:00', NULL, '177837318348996608', 'UwU', '177837092070490112'),
('2021-07-06 19:42:04', NULL, '177837335780524032', 'React', '177837092070490112'),
('2021-07-06 19:42:08', NULL, '177837351236534272', 'Gay', '177837092070490112'),
('2021-07-06 19:42:12', NULL, '177837369699860480', 'VueJS', '177837092070490112'),
('2021-07-06 19:42:19', NULL, '177837397336129536', 'Wordpress', '177837092070490112'),
('2021-07-06 19:42:24', NULL, '177837416768339968', 'NodeJS', '177837092070490112'),
('2021-07-06 19:43:08', NULL, '177837604186619904', 'Consanguins', '177837092070490112');

-- --------------------------------------------------------

--
-- Table structure for table `guild_image`
--

CREATE TABLE `guild_image` (
  `id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `format` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `guildId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `guild_member`
--

CREATE TABLE `guild_member` (
  `createdAt` datetime NOT NULL,
  `updateAt` datetime DEFAULT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `joinAt` datetime NOT NULL,
  `guildId` varchar(255) DEFAULT NULL,
  `userId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `guild_member`
--

INSERT INTO `guild_member` (`createdAt`, `updateAt`, `id`, `name`, `joinAt`, `guildId`, `userId`) VALUES
('2021-07-06 19:41:47', NULL, 1, 'John Titor', '2021-07-06 19:41:47', '177837262963212288', '177837092070490112'),
('2021-07-06 19:41:55', NULL, 2, 'John Titor', '2021-07-06 19:41:55', '177837295204827136', '177837092070490112'),
('2021-07-06 19:42:00', NULL, 3, 'John Titor', '2021-07-06 19:42:00', '177837318348996608', '177837092070490112'),
('2021-07-06 19:42:04', NULL, 4, 'John Titor', '2021-07-06 19:42:04', '177837335780524032', '177837092070490112'),
('2021-07-06 19:42:08', NULL, 5, 'John Titor', '2021-07-06 19:42:08', '177837351236534272', '177837092070490112'),
('2021-07-06 19:42:13', NULL, 6, 'John Titor', '2021-07-06 19:42:13', '177837369699860480', '177837092070490112'),
('2021-07-06 19:42:19', NULL, 7, 'John Titor', '2021-07-06 19:42:19', '177837397336129536', '177837092070490112'),
('2021-07-06 19:42:24', NULL, 8, 'John Titor', '2021-07-06 19:42:24', '177837416768339968', '177837092070490112'),
('2021-07-06 19:43:08', NULL, 9, 'John Titor', '2021-07-06 19:43:08', '177837604186619904', '177837092070490112');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `content` varchar(50) NOT NULL,
  `userId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `createdAt` datetime NOT NULL,
  `updateAt` datetime DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `birthday` datetime DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` text NOT NULL,
  `roles` varchar(255) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT 0,
  `lastLogin` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`createdAt`, `updateAt`, `id`, `firstname`, `lastname`, `username`, `birthday`, `email`, `password`, `salt`, `roles`, `enabled`, `lastLogin`) VALUES
('2021-07-06 19:41:06', '2021-07-06 19:41:21', '177837092070490112', 'John', 'Titor', 'John Titor', NULL, 'john.titor@mail.io', '$argon2i$v=19$m=4096,t=3,p=1$boiMufbJsF+VD98Rzw1peKm4uJAb1nkrwxeUWENYbwM$/zZRF27gMc7PO0ibp9YkiKQAcs4AcZauuHHpehkiraw', '6e888cb9f6c9b05f950fdf11cf0d6978a9b8b8901bd6792bc317945843586f03', 'SUPER_ADMIN', 1, NULL),
('2021-07-06 19:41:26', '2021-07-06 19:41:30', '177837172739538944', 'Pain', 'D\'épice', 'Pain D\'épice', NULL, 'pain.depice@mail.io', '$argon2i$v=19$m=4096,t=3,p=1$qYPEQMxEB4E1As/flgxqZLQmXspf7AapC/pXM8o+lz0$38X52skO9vupyV37kkg2ZQvHn89Recdg9mn/q0bn8wg', 'a983c440cc4407813502cfdf960c6a64b4265eca5fec06a90bfa5733ca3e973d', 'SUPER_ADMIN', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_banned`
--

CREATE TABLE `user_banned` (
  `id` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `banned` tinyint(4) NOT NULL DEFAULT 0,
  `bannedAt` datetime NOT NULL,
  `endBannedAt` datetime NOT NULL,
  `userId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_image`
--

CREATE TABLE `user_image` (
  `id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `format` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `verification_token`
--

CREATE TABLE `verification_token` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `expiryDate` datetime NOT NULL,
  `userId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `channel`
--
ALTER TABLE `channel`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_58d968d578e6279e2cc884db403` (`guildId`);

--
-- Indexes for table `friend`
--
ALTER TABLE `friend`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_855044ea856e46f62a46acebd65` (`userId`),
  ADD KEY `FK_d9bf438025ff9f7ae947596b38e` (`friendId`);

--
-- Indexes for table `guild`
--
ALTER TABLE `guild`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_66f23cc7e032e3ab40b5424e68b` (`ownerId`);

--
-- Indexes for table `guild_image`
--
ALTER TABLE `guild_image`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `REL_c97b0cb9ffba484138ea119157` (`guildId`);

--
-- Indexes for table `guild_member`
--
ALTER TABLE `guild_member`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_c6adfed3d6a7330d91a4f21ce1d` (`guildId`),
  ADD KEY `FK_079c272d9c6d2ccc45240ff246f` (`userId`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `REL_94cb5dda3cf592da917ec3a274` (`userId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`);

--
-- Indexes for table `user_banned`
--
ALTER TABLE `user_banned`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_94b8d0127f2a7ce5ddefbe090c3` (`userId`);

--
-- Indexes for table `user_image`
--
ALTER TABLE `user_image`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `REL_bbbdf8daa06964389b3f90b9c2` (`userId`);

--
-- Indexes for table `verification_token`
--
ALTER TABLE `verification_token`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_0748c047a951e34c0b686bfadb2` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `friend`
--
ALTER TABLE `friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guild_image`
--
ALTER TABLE `guild_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `guild_member`
--
ALTER TABLE `guild_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_banned`
--
ALTER TABLE `user_banned`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_image`
--
ALTER TABLE `user_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `verification_token`
--
ALTER TABLE `verification_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `channel`
--
ALTER TABLE `channel`
  ADD CONSTRAINT `FK_58d968d578e6279e2cc884db403` FOREIGN KEY (`guildId`) REFERENCES `guild` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `friend`
--
ALTER TABLE `friend`
  ADD CONSTRAINT `FK_855044ea856e46f62a46acebd65` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_d9bf438025ff9f7ae947596b38e` FOREIGN KEY (`friendId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `guild`
--
ALTER TABLE `guild`
  ADD CONSTRAINT `FK_66f23cc7e032e3ab40b5424e68b` FOREIGN KEY (`ownerId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `guild_image`
--
ALTER TABLE `guild_image`
  ADD CONSTRAINT `FK_c97b0cb9ffba484138ea1191573` FOREIGN KEY (`guildId`) REFERENCES `guild` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `guild_member`
--
ALTER TABLE `guild_member`
  ADD CONSTRAINT `FK_079c272d9c6d2ccc45240ff246f` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_c6adfed3d6a7330d91a4f21ce1d` FOREIGN KEY (`guildId`) REFERENCES `guild` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `status`
--
ALTER TABLE `status`
  ADD CONSTRAINT `FK_94cb5dda3cf592da917ec3a2746` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_banned`
--
ALTER TABLE `user_banned`
  ADD CONSTRAINT `FK_94b8d0127f2a7ce5ddefbe090c3` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_image`
--
ALTER TABLE `user_image`
  ADD CONSTRAINT `FK_bbbdf8daa06964389b3f90b9c2b` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `verification_token`
--
ALTER TABLE `verification_token`
  ADD CONSTRAINT `FK_0748c047a951e34c0b686bfadb2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
