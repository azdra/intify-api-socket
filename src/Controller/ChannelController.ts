import {Authorized, Body, JsonController, Post, Req, Res} from 'routing-controllers';
import {Request, Response} from 'express';
import Channel from '../Entity/Channel/Channel';
import {getRepository} from 'typeorm';
import Guild from '../Entity/Guild/Guild';

/**
 * ChannelController
 */
@Authorized()
@JsonController('/channels')
class ChannelController {
  private channelRepository = getRepository(Channel);
  private guildRepository = getRepository(Guild);

  /**
   * @param {Channel} channel
   * @param {Response} response
   * @param {Request} request
   * @return {Response}
   */
  @Post('/')
  async createChannel(
    @Body() channel: Channel,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    const guild = await this.guildRepository.findOne({
      id: String(channel.guild),
    });

    if (!guild) {
      return response.status(404).json({
        code: 'guildNotFount',
        message: 'Guild not Found',
      });
    }

    if (
      (channel.type !== Channel.TYPE_TEXT) &&
      (channel.type !== Channel.TYPE_VOICE) &&
      (channel.type !== Channel.TYPE_CATEGORY)
    ) {
      return response.status(406).json({
        code: 'typeInvalid',
        message: 'Channel type is invalid',
      });
    }

    channel.guild = guild;

    await this.channelRepository.save(channel);

    return response.json({
      code: 'channelSuccessfullyCreated',
      message: 'The channel has been successfully created',
    });
  }
}

export {ChannelController};
