import {Authorized, Body, CurrentUser, Delete, Get, JsonController, Param, Post, Req, Res} from 'routing-controllers';
import {Request, Response} from 'express';
import Message from '../Entity/Message/Message';
import {getCustomRepository, getRepository} from 'typeorm';
import Guild from '../Entity/Guild/Guild';
import Channel from '../Entity/Channel/Channel';
import User from '../Entity/User/User';
import {getPublicDir} from '../app';
import MessageAttachment from '../Entity/Message/MessageAttachment';
import GuildMemberRepository from '../Repository/GuildMemberRepository';
import {MessageRepository} from '../Repository/MessageRepository';
import * as fs from 'fs';

/**
 * Controller Messages
 */
@Authorized()
@JsonController('/messages')
class MessageController {
  private messageRepository = getCustomRepository(MessageRepository);
  private guildMemberRepository = getCustomRepository(GuildMemberRepository);
  private guildRepository = getRepository(Guild);
  private channelRepository = getRepository(Channel);
  private messageAttachmentRepository = getRepository(MessageAttachment);

  /**
   * @param {Request} request
   * @param {Response} response
   * @param {Message} message
   * @param {User} currentUser
   */
  @Post('/')
  async createMessages(
    @Req() request: Request,
    @Res() response: Response,
    @Body() message: Message,
    @CurrentUser({required: true}) currentUser: User,
  ) {
    const guild = await this.guildRepository.findOne({id: String(message.guild)});

    if (!message.guild || !guild) {
      return response.json({
        'guildInvalid': 'guildInvalid',
      });
    }

    const channel = await this.channelRepository.findOne({id: String(message.channel)});

    if (!message.channel || !channel) {
      return response.json({
        'channelInvalid': 'channelInvalid',
      });
    }

    const user = await this.guildMemberRepository.findMemberByGuildAndUser(message.guild, currentUser);

    if (!message.member || !user) {
      return response.json({
        'userInvalid': 'userInvalid',
      });
    }

    if (!request.files) {
      if (!message.content) {
        return response.json({
          'contentCantNotBeNull': 'Content can\'t not be null',
        });
      }
    }

    message.member = user;
    await this.messageRepository.save(message);

    const _files: any = request.files;

    if (_files) {
      const file = _files.file;
      const messageAttachment = new MessageAttachment();
      const path = `attachment/${message.channel}/${message.id}/${file.name}`;

      messageAttachment.format = file.mimetype;
      messageAttachment.size = file.size;
      messageAttachment.path = path;
      messageAttachment.name = file.name;
      messageAttachment.message = message;
      file.mv(`${getPublicDir()}/${path}`);
      await this.messageAttachmentRepository.save(messageAttachment);
      message.messageAttachment = [messageAttachment];
    }
    return response.json({
      message,
      user,
    });
  }

  /**
   * @param {string} guild
   * @param {string} channel
   * @param {Request} request
   * @param {Response} response
   * @return {Response}
   */
  @Get('/:guild/:channel')
  async getMessageByChannel(
    @Param('guild') guild: string,
    @Param('channel') channel: string,
    @Req() request: Request,
    @Res() response: Response,
  ) {
    const qb = await this.messageRepository.findMessageByGuildAndChannel(guild, channel);
    return response.json(qb);
  }

  /**
   * @param {string} guild
   * @param {string} channel
   * @param {string} messageID
   * @param {Response} response
   */
  @Delete('/:guild/:channel/:message')
  async deleteMessageByChannel(
    @Param('guild') guild: string,
    @Param('channel') channel: string,
    @Param('message') messageID: string,
    @Res() response: Response,
  ) {
    if (fs.existsSync(`${getPublicDir()}/attachment/${channel}/${messageID}`)) {
      console.log('FILE EXIST');
      fs.unlinkSync(`${getPublicDir()}/attachment/${channel}/${messageID}`);
    }
    return this.messageRepository.delete(messageID);
  }
}


export {MessageController};
