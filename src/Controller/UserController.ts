import {Authorized, CurrentUser, Get, JsonController, Param, Res} from 'routing-controllers';
import {Response} from 'express';
import User from '../Entity/User/User';

/**
 * Controller User
 */
@Authorized()
@JsonController('/users')
class UserController {
  /**
   * @param {Response} response
   * @param {User} currentUser
   */
  @Get('/')
  async getUserInfoByID(
    @Res() response: Response,
    @CurrentUser({required: true}) currentUser: User,
  ) {
    console.log(currentUser);
    return response.json(currentUser);
  }
}

export default UserController;
