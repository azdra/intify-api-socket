import {Body, BodyOptions, Get, JsonController, Post, Req, Res} from 'routing-controllers';
import {Request, Response} from 'express';
import {getCustomRepository, getRepository} from 'typeorm';
import User from '../Entity/User/User';
import {mailSender, sendChangePassword, sendConfirmMail, sendPasswordChanged} from '../Service/mailSender';
import TokenRepository from '../Repository/TokenRepository';
import {createVerificationToken, verificationToken} from '../Service/User/VerificationToken';
import {hashPassword} from '../Service/User/HashPassword';
import * as jwt from 'jsonwebtoken';
import {privateKey} from '../app';
import * as argon2id from 'argon2';

/**
 * AuthController
 */
@JsonController('/auth')
class AuthController {
  private userRepository = getRepository(User);
  private tokenRepository = getCustomRepository(TokenRepository)

  /**
   * Login Route
   * @param {BodyOptions} body
   * @param {Request} request
   * @param {Response} response
   * @return {Response}
   */
  @Post('/login')
  async loginUser(
    @Body() body: BodyOptions,
    @Req() request: Request,
    @Res() response: Response,
  ) {
    const {email = '', password = ''} = request.body;

    const user = await this.userRepository.findOne({
      email: email,
    });

    if (user) {
      if (!user.enabled) {
        return response.json({
          status: 401,
          code: 'accountNotEnabled',
          message: 'This account is not enabled!',
        });
      }
    }

    const checkPassword = user ? await argon2id.verify(user.password, password) : false;

    if (!checkPassword) {
      return response.json({
        status: 401,
        code: 'emailOrPasswordInvalid',
        message: 'You email/password is invalid.',
      });
    }

    const token = jwt.sign({
      id: user.id,
      email: user.email,
    }, privateKey, {
      algorithm: 'HS256',
    });

    // console.log(token);

    return response.json({
      code: 'loginAuthSuccess',
      message: 'Authentication successfully',
      user: {
        token,
        id: user.id,
      },
    });
  }

  /**
   * Register Route
   * @return {Response}
   * @param {User} user
   * @param {Request} request
   * @param {Response} response
   */
  @Post('/register')
  async registerUser(
    @Body() user: User,
    @Req() request: Request,
    @Res() response: Response,
  ) {
    const checkEmail = await this.userRepository.findOne({
      email: user.email,
    });

    // Check if the account exists
    if (checkEmail) {
      return response.json({
        'status': 409,
        'code': 'errorEmailTaken',
        'message': 'This email address has been used already.',
      });
    }

    // Generate salt and hash the password
    const crypt = await hashPassword(user.password);

    user.salt = crypt.salt.toString('hex');
    user.password = crypt.hash;

    // Insert the new USER and create the verification TOKEN
    await this.userRepository.insert(user);
    const token = await createVerificationToken(user, 'ACCOUNT_VERIFY');

    // Send the mail
    return mailSender.sendMail(sendConfirmMail(user, token)).then(() => {
      return response.json({
        code: 'accountConfirmEMailSend',
        message: 'The email of confirmation has been send.',
      });
    });
  }

  /**
   * Verify Email Route
   * @return {Response}
   * @param {Request} request
   * @param {Response} response
   */
  @Get('/email/verify')
  async verifyEmail(
    @Req() request: Request,
    @Res() response: Response,
  ) {
    const {token} = request.query;

    if (!token) {
      return response.json({
        status: 404,
        code: 'missingToken',
        message: 'Missing token params in request.',
      });
    }

    const tokenUser = await this.tokenRepository.findUserByToken(String(token));

    const verifyToken = await verificationToken(tokenUser, response);

    if (verifyToken) {
      if (verifyToken instanceof User) {
        return this.userRepository.save(verifyToken).then(() => {
          return response.json({
            status: 401,
            code: 'accountSuccessEnabled',
            message: 'The account has been successfully enabled.',
          });
        });
      }
    }
  }

  /**
   * @param {body} body
   * @param {request} request
   * @param {response} response
   */
  @Post('/password/change')
  async passwordChange(
    @Body() body: BodyOptions,
    @Req() request: Request,
    @Res() response: Response,
  ) {
    const {email} = request.body;

    if (!email) {
      return response.json({
        status: 404,
        code: 'emailNotGiven',
        message: 'The email was not given.',
      });
    }

    const user = await this.userRepository.findOne({
      email: email,
    });

    if (user) {
      const vToken = await createVerificationToken(user, 'PASSWORD_RESET');
      await mailSender.sendMail(sendChangePassword(user, vToken));
    }

    return response.json({
      status: 201,
      code: 'emailPasswordChangeSend',
      message: 'The mail for change the password has been send',
    });
  }

  /**
   * @param {body} body
   * @param {request} request
   * @param {response} response
   */
  @Post('/password/reset')
  async passwordReset(
    @Body() body: BodyOptions,
    @Req() request: Request,
    @Res() response: Response,
  ) {
    // eslint-disable-next-line no-unused-vars
    const {password} = request.body;
    const {token} = request.query;

    if (!token) {
      return response.json({
        status: 404,
        code: 'missingToken',
        message: 'Missing token params in request.',
      });
    }

    const tokenUser = await this.tokenRepository.findUserByToken(String(token));

    const user = await verificationToken(tokenUser, response);

    if (user) {
      if (user instanceof User) {
        // Generate salt and hash the password
        const crypt = await hashPassword(password);
        user.salt = crypt.salt.toString('hex');
        user.password = crypt.hash;

        // Save the modification
        await this.userRepository.save(user);

        response.json({
          code: 'accountPasswordReset',
          message: 'The password of you account has been changed.',
        });

        await mailSender.sendMail(sendPasswordChanged(user));
      }
    }
  }
}

export {AuthController};
