import {Authorized, Body, CurrentUser, Get, JsonController, Param, Post, Req, Res} from 'routing-controllers';
import {Request, Response} from 'express';
import Guild from '../Entity/Guild/Guild';
import {getCustomRepository, getRepository} from 'typeorm';
import User from '../Entity/User/User';
import GuildRepository from '../Repository/GuildRepository';

/**
 * Controller Guilds
 */
@Authorized()
@JsonController('/guilds')
class GuildController {
  private guildRepository = getCustomRepository(GuildRepository);
  private userRepository = getRepository(User);

  /**
   * @param {Guild} guild
   * @param {Request} request
   * @param {Response} response
   * @return {Response}
   */
  @Post('/')
  async createGuild(
    @Body() guild: Guild,
    @Req() request: Request,
    @Res() response: Response,
  ) {
    const {owner = false} = request.body;

    const user = await this.userRepository.findOne({
      id: owner,
    });

    if (!user) {
      return response.status(404).json({
        code: 'userNotFound',
        message: 'User not found with this ID!',
      });
    }

    if (user) {
      guild.owner = user;
      await this.guildRepository.insert(guild);
      await guild.addMember(user);
    }

    return response.json({
      code: 'guildSuccessCreated',
      message: 'The guild has been successfully created!',
    });
  }

  /**
   * @param {Response} response
   * @param {User} currentUser
   */
  @Get('/users')
  async getGuildPerUser(
    @Res() response: Response,
    @CurrentUser({required: true}) currentUser: User,
  ) {
    const data = await this.guildRepository.findGuildPerUser(currentUser.id);
    return response.json(data);
  }

  @Get('/:guildID')
  async getSpecificGuild(
    @Res() response: Response,
    @CurrentUser({required: true}) currentUser: User,
    @Param('guildID') guildID: string,
  ) {
    const qb = await this.guildRepository.createQueryBuilder('guild')
        .leftJoinAndSelect('guild.members', 'members')
        .leftJoinAndSelect('guild.image', 'image')
        .leftJoinAndSelect('guild.channels', 'channels')
        .andWhere('guild.id = :guildID')
        .setParameters({
          guildID: guildID,
        })
        .getOne()
    ;
    return response.json(qb);
  }
}

export {GuildController};
