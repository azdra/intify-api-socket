import {
  Authorized,
  Body,
  BodyOptions,
  JsonController,
  Param,
  Post, Req, Res,
} from 'routing-controllers';
import {getCustomRepository, getRepository} from 'typeorm';
import User from '../Entity/User/User';
import Friend from '../Entity/User/Friend';
import {Request, Response} from 'express';
import FriendRepository from '../Repository/FriendRepository';

/**
 * Controller Friends
 */
@Authorized()
@JsonController('/friends')
class FriendController {
  private userRepository = getRepository(User);
  private friendRepository = getCustomRepository(FriendRepository);

  /**
   * @param {string} id
   * @param {BodyOptions} body
   * @param {Request} request
   * @param {Response} response
   */
  @Post('/:id')
  async addFriends(
    @Param('id') id: string,
    @Body() body: BodyOptions,
    @Req() request: Request,
    @Res() response: Response,
  ) {
    const {friend} = request.body;
    const users = await this.userRepository.findByIds([id, friend]);

    const newFriend = new Friend();
    newFriend.user = users[0];
    newFriend.friend = users[1];

    /* const newFriend = new Friend();
    newFriend.user = users[1];
    newFriend.friend = users[0];
    await this.friendRepository.save([newFriend1, newFriend]);*/

    await this.friendRepository.save(newFriend);

    return response.json({
      code: 'friendRequestSent',
      message: 'The friend request has been sent.',
    });
  }

  /** [
   * @param {string} id
   * @param {BodyOptions} body
   * @param {Request} request
   * @param {Response} response
   */
  @Post('/:id/accepted')
  async acceptFriendsRequest(
    @Param('id') id: string,
    @Body() body: BodyOptions,
    @Req() request: Request,
    @Res() response: Response,
  ) {
    /* const {friend} = request.body;
    const users = await this.userRepository.findByIds([id, friend]);
    const friendRequest = await this.friendRepository.rechercherMachinParTruc(users[0].id, users[1].id);*/

    return response.json({
      code: 'friendRequestAccepted',
      message: 'The friend request has been accepted!',
    });
  }
}

export {FriendController};
