import {ExpressMiddlewareInterface, Middleware} from 'routing-controllers';
import {NextFunction, Request, Response} from 'express';

/**
 * Class NotFoundMiddleware
 */
@Middleware({type: 'after'})
class NotFoundMiddleware implements ExpressMiddlewareInterface {
  /**
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  public use(req: Request, res: Response, next?: NextFunction): void {
    if (!res.headersSent) {
      res.status(404).json({
        error: `no route found for "${req.url}"!`,
      });
    }
    res.end();
  }
}

export default NotFoundMiddleware;
