import {
  Middleware,
  ExpressErrorMiddlewareInterface,
} from 'routing-controllers';

/**
 * CLass ErrorHandler
 */
@Middleware({type: 'after'})
class ApiErrorHandlerMiddleware implements ExpressErrorMiddlewareInterface {
  /**
   * Error
   * @param {any} error
   * @param {any} request
   * @param {any} response
   * @param {any} next
   */
  public error = (
      error: any,
      request: any,
      response: any,
      next: (err: any) => any,
  ) => {
    if (response.headersSent) {
      return;
    }

    response.json(error);
  };
}

export default ApiErrorHandlerMiddleware;
