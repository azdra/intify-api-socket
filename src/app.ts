import './sentry';
import * as dotenv from 'dotenv'; dotenv.config();
import 'reflect-metadata';
import * as fileUpload from 'express-fileupload';
import * as cors from 'cors';
import * as express from 'express';
import * as fs from 'fs';
import * as bodyParser from 'body-parser';
import {Action, useExpressServer} from 'routing-controllers';
import './socket';
import * as jwt from 'jsonwebtoken';

const PORT = process.env.APP_PORT || 3001;
export const API_ROUTE = process.env.ROOT_PREFIX || '/api';
export const privateKey = fs.readFileSync(__dirname+'/..'+process.env.JWT_SECRET_KEY);
export const publicKey = fs.readFileSync(__dirname+'/..'+process.env.JWT_PUBLIC_KEY);
export const getProjectDir = (): string => __dirname;
export const getPublicDir = () => getProjectDir()+'/../public';
import DbConnect from './Service/dbConnect';
import User from './Entity/User/User';

const app = express();
app.use(express.static('public'));

app.use(fileUpload({
  createParentPath: true,
  limits: {
    fileSize: 10 * 1024 * 1024,
  },
}));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

/**
 *
 * @param {string} authorization
 * @param {boolean} isUserCheck
 */
const authMiddleware = async (authorization: string, isUserCheck: boolean) => {
  const bearerToken = authorization.split(' ')[1];
  let jwtValid: string | boolean = null;
  jwt.verify(bearerToken, privateKey, {
    algorithms: ['HS256'],
  }, (err, decodeJwt: any) => {
    if (err) return jwtValid = false;
    jwtValid = decodeJwt.id;
  });
  return jwtValid;
};

const db = new DbConnect();
db.getConnection().then((r) => {
  useExpressServer(app, {
    currentUserChecker: async (action: Action) => {
      const token = action.request.headers['authorization'];
      const authCheck = await authMiddleware(token, true);
      return r.getRepository(User).findOne({id: String(authCheck)});
    },
    authorizationChecker: async (action: Action) => {
      const token = action.request.headers['authorization'];
      return await authMiddleware(token, true) as boolean;
    },
    development: process.env.APP_DEV.toString() === 'true',
    defaultErrorHandler: false,
    middlewares: [__dirname+'/Middleware/**/*.ts'],
    routePrefix: API_ROUTE,
    controllers: [__dirname+'/Controller/**/*.ts'],
    cors: true,
  }).listen(PORT, () => {
    console.log('API started on localhost:%s', PORT+API_ROUTE);
  });
});

