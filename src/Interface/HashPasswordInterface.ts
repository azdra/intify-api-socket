export interface HashPassword {
  salt: Buffer;
  hash: string;
}
