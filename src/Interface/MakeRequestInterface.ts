export interface MakeRequestInterface {
  method: 'GET'|'POST'|'DELETE';
  url: string;
  data?: any;
  headers?: any;
}
