import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import User from './User';

/**
 * class UserBanned
 */
@Entity()
class UserBanned {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  reason: string;

  @Column({default: 0})
  banned: boolean

  @Column()
  bannedAt: Date

  @Column()
  endBannedAt: Date

  @ManyToOne(() => User, (user) => user.userBanneds)
  @JoinColumn()
  user: User;
}

export default UserBanned;
