import {
  Entity, JoinColumn,
  OneToOne,
} from 'typeorm';
import User from './User';
import Image from '../Image';

/**
 * Entity UserImage
 */
@Entity()
class UserImage extends Image {
  @OneToOne(() => User, (user) => user.image)
  @JoinColumn()
  user: User
}

export default UserImage;
