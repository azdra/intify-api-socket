import {
  BaseEntity,
  BeforeInsert,
  Column,
  Entity, JoinColumn, ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';

/**
 * Entity UserVerification
 */
@Entity()
class VerificationToken extends BaseEntity {
  expiration: number = 10 * 60 * 1000;

  ACCOUNT_VERIFY: number = 1
  PASSWORD_RESET: number = 2

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  token: string;

  @Column()
  type: number;

  @Column()
  expiryDate: Date;

  @ManyToOne(() => User, {onDelete: 'CASCADE'})
  @JoinColumn()
  user: User;

  /**
   * beforeInsert
   * @private
   */
  @BeforeInsert()
  private beforeInsert() {
    this.expiryDate = this.generateExpiryDate();
  }

  /**
   * @return {Date}
   */
  private generateExpiryDate = (): Date => {
    const cal = new Date();
    cal.setTime(cal.getTime()+this.expiration);
    return new Date(cal);
  }
}

export default VerificationToken;
