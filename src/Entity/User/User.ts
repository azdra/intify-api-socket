import {
  BeforeInsert,
  Entity,
  OneToOne,
  PrimaryColumn,
  Column,
  OneToMany,
} from 'typeorm';
import {generateId} from '../../Service/generateId';
import Status from './Status';
import UserImage from './UserImage';
import VerificationToken from './VerificationToken';
import UserBanned from './UserBanned';
import Friend from './Friend';
import BaseEntity from '../BaseEntity';
import Guild from '../Guild/Guild';
import GuildMember from '../Guild/GuildMember';

/**
 * Entity User
 */
@Entity()
class User extends BaseEntity {
  @PrimaryColumn()
  id: string

  @Column()
  firstname: string;

  @Column()
  lastname: string;

  @Column()
  username: string;

  @Column({nullable: true})
  birthday?: Date;

  @Column({unique: true})
  email: string;

  @Column()
  password: string;

  @Column({type: 'text'})
  salt: string;

  @Column()
  roles: string

  @Column({type: 'boolean', default: 0})
  enabled: boolean

  @Column({type: 'timestamp', nullable: true})
  lastLogin?: Date;

  @OneToMany(() => Guild, (server) => server.owner)
  serverOwners?: Guild[];

  @OneToMany(() => GuildMember, (guildMember) => guildMember.user, {onDelete: 'CASCADE'})
  guildMembers?: GuildMember[];

  @OneToOne(() => Status, (status) => status.user)
  status?: Status

  @OneToMany(() => Friend, (user) => user.user)
  userFriends?: User[];

  @OneToMany(() => Friend, (user) => user.friend)
  friends?: User[];

  @OneToOne(() => UserImage, (image) => image.user)
  image?: UserImage;

  @OneToMany(() => VerificationToken, (token) => token.user)
  private verificationTokens?: VerificationToken[];

  @OneToMany(() => UserBanned, (userBanned) => userBanned.user)
  userBanneds?: UserBanned[];

  /**
   * beforeInsert
   * @private
   */
  @BeforeInsert()
  private beforeInsert() {
    this.id = generateId();
  }
}

export default User;
