import {
  Column,
  Entity, JoinColumn, OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';

/**
 * Entity User
 */
@Entity()
class Status {
  @PrimaryGeneratedColumn()
  id: number

  @Column({
    type: 'varchar',
    length: 50,
    nullable: false,
  })
  content: string

  @OneToOne(() => User, (user) => user.status)
  @JoinColumn()
  user: User
}

export default Status;
