import {
  Column,
  Entity, JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import User from './User';
import BaseEntity from '../BaseEntity';

/**
 * Entity Friend
 */
@Entity()
class Friend extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({default: 0})
  accepted: boolean;

  @ManyToOne(() => User, (user) => user.userFriends)
  @JoinColumn()
  user: User;

  @ManyToOne(() => User, (user) => user.friends)
  @JoinColumn()
  friend: User;
}

export default Friend;
