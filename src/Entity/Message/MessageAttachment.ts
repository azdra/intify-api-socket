import {Entity, ManyToOne} from 'typeorm';
import Image from '../Image';
import Message from './Message';

/**
 * Entity MessageAttachment
 */
@Entity()
class MessageAttachment extends Image {
  @ManyToOne(() => Message, (message) => message.messageAttachment, {onDelete: 'CASCADE'})
  message: Message
}

export default MessageAttachment;
