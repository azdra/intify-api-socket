import {
  BeforeInsert,
  Column,
  Entity,
  JoinColumn, JoinTable, ManyToOne, OneToMany,
  PrimaryColumn,
} from 'typeorm';
import BaseEntity from '../BaseEntity';
import {generateId} from '../../Service/generateId';
import Guild from '../Guild/Guild';
import Channel from '../Channel/Channel';
import GuildMember from '../Guild/GuildMember';
import MessageAttachment from './MessageAttachment';

/**
 * Entity Message
 */
@Entity()
class Message extends BaseEntity {
  @PrimaryColumn()
  id: string;

  @ManyToOne(() => Guild, (guild) => guild.message)
  @JoinColumn()
  guild: Guild;

  @ManyToOne(() => Channel, (channel) => channel.message)
  channel: Channel;

  @ManyToOne(() => GuildMember, (user) => user.message)
  member: GuildMember;

  @Column({type: 'text'})
  content: string;

  @OneToMany(() => MessageAttachment, (messageAttachment) => messageAttachment.message)
  @JoinTable()
  messageAttachment?: MessageAttachment[];

  /**
   * beforeInsert
   * @private
   */
  @BeforeInsert()
  private beforeInsert() {
    this.id = generateId();
  }
}

export default Message;
