import {Column, PrimaryGeneratedColumn} from 'typeorm';

/**
 * Entity image
 */
class Image {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  path: string;

  @Column()
  size: number;

  @Column()
  format: string;

  @Column()
  name: string;
}

export default Image;
