import {Entity, JoinColumn, OneToOne} from 'typeorm';
import Image from '../Image';
import Guild from './Guild';

/**
 * Entity GuildImage
 */
@Entity()
class GuildImage extends Image {
  @OneToOne(() => Guild, (server) => server.image, {onDelete: 'CASCADE'})
  @JoinColumn()
  guild: Guild
}

export default GuildImage;
