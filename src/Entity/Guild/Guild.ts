import {
  AfterInsert,
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryColumn,
} from 'typeorm';
import {generateId} from '../../Service/generateId';
import GuildImage from './GuildImage';
import Channel from '../Channel/Channel';
import BaseEntity from '../BaseEntity';
import User from '../User/User';
import GuildMember from './GuildMember';
import Message from '../Message/Message';

/**
 * Entity Guild
 */
@Entity()
class Guild extends BaseEntity {
  @PrimaryColumn()
  id: string

  @Column({length: 100})
  name: string

  @OneToMany(() => GuildMember, (guildMember) => guildMember.guild, {onDelete: 'CASCADE'})
  members: GuildMember[];

  @ManyToOne(() => User, (user) => user.serverOwners, {onDelete: 'CASCADE'})
  @JoinColumn()
  owner: User;

  @OneToOne(() => GuildImage, (image) => image.guild, {onDelete: 'CASCADE'})
  image: GuildImage;

  @OneToMany(() => Channel, (channel) => channel.guild, {onDelete: 'CASCADE'})
  channels: Channel[];

  @OneToMany(() => Message, (message) => message.guild)
  message: Message[];

  /**
   * beforeInsert
   * @private
   */
  @BeforeInsert()
  private beforeInsert() {
    this.id = generateId();
  }

  /**
   * afterInsert
   * @private
   */
  @AfterInsert()
  private async afterInsert() {
    const categoryText = new Channel();
    categoryText.guild = this;
    categoryText.name = 'text channels';
    categoryText.type = Channel.TYPE_CATEGORY;
    await categoryText.save();

    const generalText = new Channel();
    generalText.guild = this;
    generalText.name = 'general';
    generalText.type = Channel.TYPE_TEXT;
    generalText.parent = categoryText;
    await generalText.save();

    const categoryVoice = new Channel();
    categoryVoice.guild = this;
    categoryVoice.name = 'voice channels';
    categoryVoice.type = Channel.TYPE_CATEGORY;
    await categoryVoice.save();

    const generalVoice = new Channel();
    generalVoice.guild = this;
    generalVoice.name = 'general';
    generalVoice.type = Channel.TYPE_VOICE;
    generalVoice.parent = categoryVoice;
    await generalVoice.save();
  }

  /**
   * @param {User} user
   */
  async addMember(user: User) {
    const member = new GuildMember();
    member.name = user.username;
    member.user = user;
    member.guild = this;
    await member.save();
  }
}

export default Guild;
