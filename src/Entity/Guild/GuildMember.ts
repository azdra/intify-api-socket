import {BeforeInsert, Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import Guild from './Guild';
import User from '../User/User';
import BaseEntity from '../BaseEntity';
import Message from '../Message/Message';

/**
 * Entity GuildMember
 */
@Entity()
class GuildMember extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  joinAt: Date;

  @ManyToOne(() => Guild, (guild) => guild.members, {onDelete: 'CASCADE'})
  @JoinColumn()
  guild: Guild;

  @ManyToOne(() => User, (user) => user.guildMembers, {onDelete: 'CASCADE'})
  @JoinColumn()
  user: User;

  @OneToMany(() => Message, (message) => message.member)
  message: Message[];

  /**
   * @private
   */
  @BeforeInsert()
  private setJoinAt() {
    this.joinAt = new Date();
  }
}

export default GuildMember;
