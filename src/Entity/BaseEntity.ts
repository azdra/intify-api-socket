import {BeforeInsert, BeforeUpdate, Column} from 'typeorm';
import {BaseEntity as BaseEntityTypeOrm} from 'typeorm';

/**
 * Entity BaseEntity
 */
class BaseEntity extends BaseEntityTypeOrm {
  @Column()
  createdAt: Date

  @Column({nullable: true})
  updateAt: Date

  /**
   * @private
   */
  @BeforeInsert()
  private setCreatedAt() {
    this.createdAt = new Date();
  }

  /**
   * @private
   */
  @BeforeUpdate()
  private setUpdatedAt() {
    this.updateAt = new Date();
  }
}

export default BaseEntity;
