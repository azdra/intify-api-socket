import {
  BeforeInsert, Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryColumn, OneToMany,
} from 'typeorm';
import {generateId} from '../../Service/generateId';
import Guild from '../Guild/Guild';
import BaseEntity from '../BaseEntity';
import Message from '../Message/Message';

/**
 * Entity Channel
 */
@Entity()
class Channel extends BaseEntity {
  static TYPE_DM = 'dm';
  static TYPE_TEXT = 'text';
  static TYPE_VOICE = 'voice';
  static TYPE_CATEGORY = 'category';

  @PrimaryColumn()
  id: string

  @Column({length: 100})
  name: string

  @Column({length: 255, nullable: true})
  topic: string

  @Column({default: Channel.TYPE_TEXT})
  type: string

  @Column({nullable: true})
  order: number

  @ManyToOne(() => Guild, (guild) => guild.channels, {onDelete: 'CASCADE'})
  @JoinColumn()
  guild: Guild;

  @ManyToOne(() => Channel, (channel) => channel.children)
  parent: Channel;

  @OneToMany(() => Channel, (channel) => channel.parent)
  children: Channel[];

  @OneToMany(() => Message, (message) => message.channel)
  message: Message[];

  /**
   * beforeInsert
   * @private
   */
  @BeforeInsert()
  private beforeInsert() {
    this.id = generateId();
  }
}

export default Channel;
