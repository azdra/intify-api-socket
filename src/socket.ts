import {toHTML} from 'discord-markdown';
import {MakeRequestInterface} from './Interface/MakeRequestInterface';
import {Server} from 'socket.io';
import axios, {AxiosPromise, AxiosResponse} from 'axios';
import Message from './Entity/Message/Message';
import {createServer} from 'http';

const PORT = 3002;
export const makeRequest = (config: MakeRequestInterface): AxiosPromise => {
  return axios({
    method: config.method,
    url: config.url,
    baseURL: 'http://localhost:3001/api/v1',
    data: config.data,
    headers: config.headers,
  });
};

const httpServer = createServer();
export const io = new Server(httpServer, {
  cors: {
    methods: ['GET', 'POST'],
    allowedHeaders: ['my-custom-header'],
    credentials: true,
  },
});

/**
 * @param {Message[]} messages
 * @return {Message[]}
 */
const renderText = (messages: Message[]) => {
  const newMessage = messages;
  newMessage.forEach((message) => {
    message.content = toHTML(message.content);
  });
  return newMessage;
};

interface MessageDateInterface {
  guild: string;
  channel: string;
  token: string;
  message: Message;
}

// Socket io connection
io.on('connection', (socket) => {
  // Get the user guild
  socket.on('USER:GET:GUILD', (data, callback) => {
    makeRequest({
      method: 'GET',
      url: `/guilds/users`,
      headers: {
        'Authorization': `Bearer ${data.token}`,
      },
    }).then((r) => {
      callback(r.data);
      // Add socket client in the text channels/rooms
      for (const guild of r.data) {
        for (const channel of guild.channels) {
          if (channel.type === 'text') {
            socket.join(channel.id);
          }
        }
      }
    });
  });

  // Get the messages from the channels
  socket.on('GUILD:CHANNEL:GET:MESSAGE', (data, callback) => {
    makeRequest({
      method: 'GET',
      url: `/messages/${data.guildId}/${data.channelId}`,
      headers: {
        'Authorization': `Bearer ${data.token}`,
      },
    }).then((r: AxiosResponse<Message[]>) => callback(renderText(r.data)));
  });

  socket.on('GET:DATA:GUILD', (data, callback) => {
    makeRequest({
      method: 'GET',
      url: `/guilds/${data.guildID}`,
      headers: {
        'Authorization': `Bearer ${data.token}`,
      },
    }).then((r: AxiosResponse<Message[]>) => {
      console.log(r.data);
      callback(r.data);
    });
  });

  socket.on('GUILD:CHANNEL:DELETE:MESSAGE', (data: MessageDateInterface) => {
    makeRequest({
      method: 'DELETE',
      url: `/messages/${data.guild}/${data.channel}/${data.message.id}`,
      headers: {
        'Authorization': `Bearer ${data.token}`,
      },
    }).then((r) => {
      socket.emit('');
      console.log(r.data);
    });
  });

  socket.on('USER:SEND:MESSAGE', (data) => {
    console.log(data);
    return socket.emit('SERVER:SEND:MESSAGE', 'www');
    /* */
    // socket.emit('SERVER:SEND:MESSAGE', 'fsqfjqisfjqsiofjsoqi');
  });

  socket.on('@ME:GET:DATA', (data, callback) => {
    makeRequest({
      method: 'GET',
      url: `/users`,
      headers: {
        'Authorization': `Bearer ${data.token}`,
      },
    }).then((r) => {
      callback(r.data);
    });
  });
});

httpServer.listen(PORT, () => {
  console.log('Socket start on localhost:%s', PORT);
});
