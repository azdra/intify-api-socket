import {EntityRepository, Repository} from 'typeorm';
import Friend from '../Entity/User/Friend';

/** [
 * FriendRepository
 */
@EntityRepository(Friend)
class FriendRepository extends Repository<Friend> {
  rechercherMachinParTruc(userId: string, friend: string) {
    return this.createQueryBuilder('friend')
        .where('friend.user = :user')
        .andWhere('friend.friend = :friend')
        .andWhere('friend.accepted <> 1')
        .setParameters({
          user: userId,
          friend: friend,
        })
        .getOne()
    ;
  }
}

export default FriendRepository;
