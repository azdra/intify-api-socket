import {EntityRepository, Repository} from 'typeorm';
import Guild from '../Entity/Guild/Guild';
import GuildMember from '../Entity/Guild/GuildMember';

/**
 * GuildRepository
 */
@EntityRepository(Guild)
class GuildRepository extends Repository<Guild> {
  /**
   * @param {string} uId
   * @return {Promise<Guild[]>}
   */
  findGuildPerUser(uId: string): Promise<Guild[]> {
    return this.createQueryBuilder('guild')
        .leftJoinAndSelect('guild.image', 'image')
        .leftJoinAndSelect('guild.channels', 'channels')
        .leftJoin('guild.members', 'member')
        .leftJoin('member.user', 'user')
        .andWhere('user.id = :uId')
        .setParameter('uId', uId)
        .getMany()
    ;
  }
}


export default GuildRepository;
