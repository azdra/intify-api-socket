import {EntityRepository, Repository} from 'typeorm';
import VerificationToken from '../Entity/User/VerificationToken';

/**
 * UserRepository
 */
@EntityRepository(VerificationToken)
class TokenRepository extends Repository<VerificationToken> {
  /**
   * findUserByToken
   * @param {string} token
   * @return {Promise<User|null>}
   */
  findUserByToken(token: string): Promise<VerificationToken|null> {
    return this.createQueryBuilder('token')
        .leftJoinAndSelect('token.user', 'user')
        .where('token.token = :token')
        .setParameter('token', String(token))
        .getOne()
    ;
  }
}

export default TokenRepository;
