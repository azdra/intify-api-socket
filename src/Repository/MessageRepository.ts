import {EntityRepository, Repository} from 'typeorm';
import Message from '../Entity/Message/Message';

/**
 * Repository Message
 */
@EntityRepository(Message)
class MessageRepository extends Repository<Message> {
  /**
   * findMessageByGuildAndChannel
   * @param {Guild} guild
   * @param {Channel} channel
   * @return {Promise<Message[]>}
   */
  findMessageByGuildAndChannel(guild: string, channel: string): Promise<Message[]> {
    return this.createQueryBuilder('message')
        .leftJoinAndSelect('message.messageAttachment', 'messageAttachment')
        .leftJoin('message.guild', 'guild')
        .leftJoin('message.channel', 'channel')
        .leftJoinAndSelect('message.member', 'member')
        .leftJoin('member.user', 'user')
        .leftJoinAndSelect('user.image', 'image')
        .addSelect(['user.id', 'user.username'])
        .andWhere('guild.id = :gId')
        .where('channel.id = :cId')
        .orderBy('message.createdAt', 'ASC')
        .setParameters({
          gId: String(guild),
          cId: String(channel),
        })
        .getMany()
    ;
  }

  /**
   * findMessageWithImage
   * @param {string} message
   * @return {Promise<Message|null>}
   */
  findMessageWithImage(message: string): Promise<Message|null> {
    return this.createQueryBuilder('message')
        .leftJoinAndSelect('message.messageAttachment', 'messageAttachment')
        .getOne()
    ;
  }
}

export {MessageRepository};
