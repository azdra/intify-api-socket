import {EntityRepository, Repository} from 'typeorm';
import GuildMember from '../Entity/Guild/GuildMember';
import Guild from '../Entity/Guild/Guild';
import User from '../Entity/User/User';

/**
 * Repository GuildMember
 */
@EntityRepository(GuildMember)
class GuildMemberRepository extends Repository<GuildMember> {
  /**
   * findMemberByGuildAndUser
   * @param {Guild} guild
   * @param {User} user
   * @return {Promise<GuildMember|null>}
   */
  findMemberByGuildAndUser(guild: Guild, user: User): Promise<GuildMember|null> {
    return this.createQueryBuilder('member')
        .leftJoin('member.guild', 'guild')
        .leftJoin('member.user', 'user')
        .leftJoin('user.image', 'image')
        .andWhere('guild.id = :gId')
        .where('user.id = :uId')
        .addSelect(['user.id', 'user.username'])
        .setParameters({
          gId: String(guild.id),
          uId: String(user.id),
        })
        .getOne()
    ;
  }
}

export default GuildMemberRepository;
