import {Connection, createConnection} from 'typeorm';
import {getProjectDir} from '../app';

/**
 * class DbConnect
 */
class DbConnect {
  private type = 'mariadb';
  private host = 'localhost'; // Or the name of the docker container
  private port = parseInt(process.env.MYSQL_PORT);
  private username = process.env.MYSQL_USER;
  private password = process.env.MYSQL_PASSWORD;
  private database = process.env.MYSQL_DATABASE;
  private synchronize = true;
  private logging = false;

  private readonly connection = null;

  /**
   * Constructor of class DbConnect
   */
  constructor() {
    this.connection = this.initConnection();
  }

  public getConnection = async (): Promise<Connection> => {
    return this.connection;
  }

  private initConnection = () => {
    return createConnection({
      'type': 'mariadb',
      'host': this.host, // Or the name of the docker container
      'port': this.port,
      'username': this.username,
      'password': this.password,
      'database': this.database,
      'synchronize': this.synchronize,
      'logging': this.logging,
      'entities': [getProjectDir()+'/Entity/**/*.ts'],
      'charset': 'utf8mb4_unicode_ci',
    });
  }
}

export default DbConnect;
