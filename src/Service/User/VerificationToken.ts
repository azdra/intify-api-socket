import User from '../../Entity/User/User';
import VerificationToken from '../../Entity/User/VerificationToken';
import {v4 as uuidv4} from 'uuid';
import * as md5 from 'md5';
import {Response} from 'express';
import {VerificationTokenType} from '../../Interface/VerificationTokenInterface';
import {mailSender, sendConfirmMail} from '../mailSender';

/**
 * @param {User} user
 * @param {VerificationTokenType} type
 * @return {Promise<string>}
 */
export const createVerificationToken = async (user: User|null, type: VerificationTokenType): Promise<string> => {
  const token: string = md5(uuidv4());

  const vToken = new VerificationToken();

  if (!vToken[type]) {
    return 'token type is not defined';
  }

  vToken.token = token;
  vToken.expiryDate = new Date();
  vToken.user = user;
  vToken.type = vToken[type];
  await vToken.save();

  return token;
};

/**
 * @param {VerificationToken} token
 * @param {Response} response
 * @return {Promise<Response|User>}
 */
export const verificationToken = async (
    token: VerificationToken,
    response: Response,
): Promise<Response|User> => {
  if (!token) {
    return response.status(401).json({
      code: 'invalidToken',
      message: 'The token is invalid.',
    });
  }
  const isAccountVerify = token.type === token.ACCOUNT_VERIFY;

  const user = token.user;
  if (isAccountVerify) {
    if (user.enabled) {
      return response.status(409).json({
        code: 'accountAlreadyEnabled',
        message: 'This account is already enabled.',
      });
    }
  }

  const expiryDate = token.expiryDate.getTime();
  const now = new Date().getTime();

  await token.remove();

  if ((expiryDate - now) <= 0) {
    const token = await createVerificationToken(user, isAccountVerify ? 'ACCOUNT_VERIFY' : 'PASSWORD_RESET');
    // Send the mail
    return mailSender.sendMail(sendConfirmMail(user, token)).then(() => {
      return response.status(404).json({
        code: 'tokenExpired',
        message: 'The token has expired. A new token has been sent',
      });
    });
  }

  if (isAccountVerify) user.enabled = true;
  return user;
};


