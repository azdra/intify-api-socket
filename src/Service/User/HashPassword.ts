import {HashPassword} from '../../Interface/HashPasswordInterface';
import * as argon2id from 'argon2';
import * as crypto from 'crypto';

/**
 * Generate salt and hash the password
 * @param {string} password
 * @return {Promise<HashPassword>}
 */
export const hashPassword = async (password: string): Promise<HashPassword> => {
  const salt = crypto.randomBytes(32);
  const hash = await argon2id.hash(password, {
    salt: salt,
  });

  return {
    salt,
    hash,
  };
};
