import * as intFormat from 'biguint-format';
import * as FlakeId from 'flake-idgen';

export const generateId = () => {
  const flake = new FlakeId({
    epoch: new Date('2020-03-03').getTime(),
  });
  return intFormat(flake.next(), 'dec');
};
