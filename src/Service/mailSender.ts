import * as nodemailer from 'nodemailer';
import User from '../Entity/User/User';
import * as Mail from 'nodemailer/lib/mailer';

export const noReplayMail = 'noreply@intify.com';

export const mailSender = nodemailer.createTransport({
  port: 1025,
  ignoreTLS: true,
});

export const sendConfirmMail = (user: User, token: string): Mail.Options => {
  return {
    from: noReplayMail,
    to: user.email,
    subject: 'Intify - Account Confirm Creation',
    html: `
        <a href="http://localhost:3001/api/v1/auth/email/verify?token=${token}">Click here to confirm your account</a>
      `,
  };
};

export const sendChangePassword = (user: User, token: string): Mail.Options => {
  return {
    from: noReplayMail,
    to: user.email,
    subject: 'Intify - Request Password Reset',
    html: `
        <a href="http://localhost:3001/api/v1/auth/password/resettoken=${token}">Click here to reset your password</a>
      `,
  };
};

export const sendPasswordChanged = (user: User): Mail.Options => {
  return {
    from: noReplayMail,
    to: user.email,
    subject: 'Intify - Password Changed',
    text: `Hi ${user.username}!\n\nThis email is to notify you that the password for the Intify account has been changed.`,
  };
};
